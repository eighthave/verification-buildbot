#!/bin/sh -ex

#buildbot_version=$(dpkg-query --showformat='${Version}' --show buildbot | cut -d - -f 1)
buildbot_version=3.7.0

# use the package from Debian/testing to make sure it'll work for us in the long term
dpkg -l python3-podman

python3 -m venv --clear --system-site-packages ~/buildbot/venv
. ~/buildbot/venv/bin/activate
pip3 install wheel
pip3 install --upgrade \
    buildbot-console-view==${buildbot_version} \
    buildbot-grid-view==${buildbot_version} \
    buildbot-waterfall-view==${buildbot_version} \
    buildbot-www==${buildbot_version} \
    buildbot==${buildbot_version}

printf "\nRun:\nsource ~/buildbot/venv/bin/activate\n"
