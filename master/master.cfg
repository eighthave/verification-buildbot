# -*- python -*-
# ex: set filetype=python:

import glob
import os
from buildbot.plugins import changes, schedulers, steps, worker, util

c = BuildmasterConfig = {}

####### WORKERS

basedir = os.path.abspath(os.path.join(os.path.dirname(__file__), '..'))
workernames = [
    os.path.basename(i).replace('-worker', '')
    for i in glob.glob(os.path.join(basedir, '*-worker'))
]
c['workers'] = [
    worker.Worker(name, "Ban9xo4jieTh7Ku3xookocheeyaegei3") for name in workernames
]
c['protocols'] = {"pb": {"port": "tcp:9989:interface=127.0.0.1"}}


####### CHANGESOURCES

c['change_source'] = [
    changes.PBChangeSource(
        port='tcp:9999:interface=127.0.0.1',
        user='verify',
        passwd='ohl3acuD0looCiengob3abae3eiquaec',
    ),
]


####### SCHEDULERS

c['schedulers'] = [
    schedulers.SingleBranchScheduler(
        name="build",
        change_filter=util.ChangeFilter(branch="master"),
        treeStableTimer=None,
        builderNames=["buildserver"],
    ),
    schedulers.ForceScheduler(
        name="force",
        builderNames=["buildserver"],
    ),
]


####### BUILDERS

home_fdroid = '/home/fdroid'
fdroid = os.path.join(home_fdroid, 'fdroidserver/fdroid')
srclibs_lock = util.MasterLock("srclibs")


def get_appid_vercode():
    return util.Interpolate('%(prop:packageName)s:%(prop:versionCode)s')


class FdroiddataShellCommand(steps.ShellCommand):
    def __init__(self, **kwargs):
        """Set up for running shell commands in /home/fdroid/fdroiddata

        The environment that buildbot provides is deliberately quite
        minimal with no environment variables.

        """
        kwargs['workdir'] = os.path.join(home_fdroid, 'fdroiddata')
        kwargs['haltOnFailure'] = True
        kwargs['description'] = 'Running ' + kwargs.get('name')
        kwargs['descriptionDone'] = kwargs.get('name')

        if not kwargs.get('command'):
            kwargs['command'] = [
                fdroid,
                kwargs['name'],
                '--verbose',
                get_appid_vercode(),
            ]
        super().__init__(**kwargs)


class InsideBuildserverCommand(FdroiddataShellCommand):
    def __init__(self, **kwargs):
        kwargs['command'] = [
            fdroid,
            'exec',
            get_appid_vercode(),
            '--',
            '/home/vagrant/fdroidserver/fdroid',
            kwargs['name'],
            '--verbose',
        ]
        if kwargs['name'] == 'scanner':
            kwargs['command'] += ['--refresh', '--exit-code']
        if kwargs.get('binary'):
            kwargs['command'].append(
                util.Interpolate(
                    'unsigned/%(prop:packageName)s_%(prop:versionCode)s.apk'
                )
            )
            kwargs['name'] += ' (binary)'
        else:
            kwargs['command'].append(get_appid_vercode())
        if 'binary' in kwargs:
            del kwargs['binary']  # super classes error on this option
        super().__init__(**kwargs)


buildserver_factory = util.BuildFactory()
buildserver_factory.addSteps(
    [
        FdroiddataShellCommand(name="up"),  # TODO supply --cpus and --memory
        FdroiddataShellCommand(name="build_inside_sudo"),
        FdroiddataShellCommand(
            name="fetchsrc", locks=[srclibs_lock.access('exclusive')]
        ),
        FdroiddataShellCommand(name="push", locks=[srclibs_lock.access('exclusive')]),
        FdroiddataShellCommand(
            name="chown temp hack",
            command=[
                fdroid,
                'exec',
                get_appid_vercode(),
                '--',
                'sudo',
                'chown',
                '-R',
                'vagrant:vagrant',
                '/home/vagrant',
            ],
        ),
        InsideBuildserverCommand(name="build_local_prepare"),
        InsideBuildserverCommand(name="scanner"),
        InsideBuildserverCommand(name="build_local_run"),
        InsideBuildserverCommand(name='scanner', binary=True),
        FdroiddataShellCommand(name="pull"),
        FdroiddataShellCommand(
            name="verify",
            command=[
                fdroid,
                'verify',
                '--clean-up-verified',
                '--reuse-remote-apk',
                '--output-json',
                '--verbose',
                util.Interpolate('%(prop:packageName)s:%(prop:versionCode)s'),
            ],
            env={'ANDROID_HOME': '/usr/lib/android-sdk'},
        ),
        FdroiddataShellCommand(name="destroy", flunkOnFailure=False, alwaysRun=True),
    ]
)


def pickNextBuild(builder, requests):
    for r in requests:
        packageName = r.properties.getProperty('packageName', '')
        if packageName.startswith('org.fdroid.'):
            return r
    return requests[0]


c['builders'] = [
    util.BuilderConfig(
        name="buildserver",
        workernames=workernames,
        factory=buildserver_factory,
        nextBuild=pickNextBuild,
    ),
]


####### BUILDBOT SERVICES

c['services'] = []


####### PROJECT IDENTITY

c['title'] = "Verification"
c['titleURL'] = "https://verification.f-droid.org/buildbot/"
c['buildbotURL'] = "https://verification.f-droid.org/buildbot/"
c['www'] = dict(
    port='tcp:8010:interface=127.0.0.1',
    plugins=dict(waterfall_view={}, console_view={}, grid_view={}),
)

####### DB URL

c['db'] = {
    # This specifies what database buildbot uses to store its state.
    # It's easy to start with sqlite, but it's recommended to switch to a dedicated
    # database, such as PostgreSQL or MySQL, for use in production environments.
    # http://docs.buildbot.net/current/manual/configuration/global.html#database-specification
    'db_url': "sqlite:///state.sqlite"
}

c['buildbotNetUsageData'] = None
