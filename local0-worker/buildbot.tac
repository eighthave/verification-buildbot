import os

from buildbot_worker.bot import Worker
from twisted.application import service

rotateLength = 10000000
maxRotatedFiles = 10

basedir = os.path.abspath(os.path.dirname(__file__))

# note: this line is matched against to check that this is a worker
# directory; do not edit it.
application = service.Application('buildbot-worker')

from twisted.python.logfile import LogFile
from twisted.python.log import ILogObserver, FileLogObserver

logfile = LogFile.fromFullPath(
    os.path.join(basedir, "twistd.log"),
    rotateLength=rotateLength,
    maxRotatedFiles=maxRotatedFiles,
)
application.setComponent(ILogObserver, FileLogObserver(logfile).emit)

buildmaster_host = '127.0.0.1'
port = 9989
workername = os.path.basename(basedir).replace('-worker', '')
passwd = 'Ban9xo4jieTh7Ku3xookocheeyaegei3'
keepalive = 600
umask = 0o0022
maxdelay = 300
numcpus = None
allow_shutdown = None
maxretries = None
use_tls = 0
delete_leftover_dirs = 0

s = Worker(
    buildmaster_host,
    port,
    workername,
    passwd,
    basedir,
    keepalive,
    umask=umask,
    maxdelay=maxdelay,
    numcpus=numcpus,
    allow_shutdown=allow_shutdown,
    maxRetries=maxretries,
    useTls=use_tls,
    delete_leftover_dirs=delete_leftover_dirs,
)
s.setServiceParent(application)
