#!/bin/sh
#
# Send a Buildbot Change event for each APK that needs to be verified.

date
set -e

cd ~/fdroiddata

. ~/buildbot/venv/bin/activate
. ~/buildbot/auth-env

# The Application IDs that should be prioritized
buildfirst="
org.fdroid.fdroid
org.fdroid.fdroid.privileged
org.fdroid.fdroid.privileged.ota
ch.protonvpn.android
com.lavabit.pahoehoe
com.tailscale.ipn
com.wireguard.android
de.blinkt.openvpn
de.schildbach.wallet
de.schildbach.wallet_test
dev.obfusk.jiten
dev.obfusk.jiten_webview
dev.obfusk.sokobang
eu.siacs.conversations
net.ivpn.client
net.mullvad.mullvadvpn
network.mysterium.vpn
nl.eduvpn.app
one.librem.tunnel
org.briarproject.briar.android
org.calyxinstitute.vpn
org.greatfire.wikiunblocked.fdroid
org.outline.android.client
org.pacien.tincapp
org.schabi.newpipe
org.strongswan.android
org.telegram.messenger
org.torproject.torservices
se.leap.bitmaskclient
se.leap.riseupvpn
"

# Sort buildfirst apps first, check if diffoscope output a .json
# already, then remove dups without sorting using awk.
appidfirst=$(for appid in $buildfirst; do ls unsigned/${appid}*.apk 2> /dev/null || true; done)
apks=$(for f in $appidfirst $(ls unsigned/*.apk); do echo $f; done | awk '!x[$0]++')
for apk in $apks; do
    test -e ${apk}.json && continue
    appid=$(printf $apk | sed 's,\s*unsigned/\(.*\)_\([0-9][0-9]*\)\.apk\s*,\1:\2,g')
    echo $appid
    packageName=$(echo $appid | cut -d : -f 1)
    versionCode=$(echo $appid | cut -d : -f 2)

    buildbot sendchange --master=127.0.0.1:9999 --auth=$auth \
	     --who $packageName \
             --branch=verify \
	     --repository="https://gitlab.com/fdroid/fdroiddata" \
	     --revision=$(git -C ~/fdroiddata describe --always) \
	     --category=verify \
	     --project=$packageName \
	     --property=versionCode:$versionCode \
	     --property=packageName:$packageName \
	     --property=unsignedApk:$apk \
	     $apk
    sleep 30
done
