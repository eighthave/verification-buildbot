
A prototype for running F-Droid using buildbot.


# verification.f-droid.org

```console
# su - fdroid
$ screen -S buildbot -L -Logfile buildbot.log
$ source venv/bin/activate
$ buildbot checkconfig master && buildbot stop master && buildbot upgrade-master master && buildbot start master && buildbot-worker restart fdroid-worker
Config file is good!
buildbot process 2348 is dead
checking basedir
checking for running master
checking master.cfg
upgrading basedir
upgrading database (sqlite:/state.sqlite)
Warning: Stopping this process might cause data loss
upgrade complete
Following twistd.log until startup finished..
The buildmaster appears to have (re)started correctly.
worker process 2356 is dead
now restarting worker process..
Following twistd.log until startup finished..
The buildbot-worker appears to have (re)started correctly.
$
```
